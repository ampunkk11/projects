from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from .models import Jadwal
from . import views
# Create your tests here.
class Tests(TestCase):
    def setUp(self):
        Jadwal.objects.create(nama_matkul = "letsgo", nama_dosen ="yeah",
        sks ="3",ruang="2402",nama_matkul_panjang="yeee",jam_kelas="rabu 14.00-15.00")
        
#url test
    def test_list_url_is_resolved(self):
        response = Client().get('/jadwal')
        self.assertEquals(response.status_code, 200)
    

    def test_list_url_is_resolved2(self):
        response = Client().get('/tambah')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved3(self):
        response = Client().get('/index')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved4(self):
        response = Client().get('/index2')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved5(self):
        response = Client().get('/index4')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved6(self):
        response = Client().get('/jadwal_detail/1/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved7(self):
        response = Client().get('/deletematkul/1/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved8(self):
        response = Client().get('/updatematkul/1/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_is_resolved9(self):
        response = Client().get('/tambahSubmisi/')
        self.assertEquals(response.status_code, 200)
        

        
#view test




    def test_match_input(self):
        func = resolve('/tambah')
        self.assertEqual(func.func, views.tambah)

    def test_match_input2(self):
        func = resolve('/tambahSubmisi/')
        self.assertEqual(func.func, views.tambahSubmisi)
