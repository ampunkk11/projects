from django.db import models

# Create your models here.
class Jadwal(models.Model):
    nama_matkul = models.CharField(max_length=30)
    nama_dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=10)
    ruang = models.CharField(max_length=30)
    nama_matkul_panjang = models.CharField(max_length=30,default='')
    jam_kelas = models.CharField(max_length=60,default='')
    
