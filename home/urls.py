from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [

    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('index2', views.index2, name='index2'),
    path('index4', views.index4, name='index4'),
    path('jadwal', views.jadwal, name='jadwal'),
    path('tambah', views.tambah, name='tambah'),
    path('tambahSubmisi/', views.tambahSubmisi, name='tambahSubmisi'),
    path('updatematkul/<int:pk>/', views.updateMatkul, name='updatematkul'),
    path('deletematkul/<int:pk>/', views.deleteMatkul, name='deletematkul'),
    path('jadwal_detail/<int:pk>/', views.jadwal_detail, name='jadwal_detail'),
]