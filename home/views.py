from django.http import HttpResponse
from django.shortcuts import render, redirect
from .models import Jadwal
from .forms import JadwalForm

# Create your views here.
def index(request):
    context = {"number":1,
                }
    return render(request, 'home/index.html',context)

def index2(request):
    context = {"number":2,
                }
    return render(request, 'home/index2.html',context)

def index4(request):
    context = {"number":3,
                }
    return render(request, 'home/index4.html',context)

def jadwal(request):
    jadwal = Jadwal.objects.all()
    context = {'jadwal':jadwal,
                'number':4}
    return render(request, 'home/jadwal.html', context) 

def jadwal_detail(request, pk):
    jadwal = Jadwal.objects.all().filter(id=pk)
    context = {'jadwal':jadwal,
                'number':4}
    return render(request, 'home/jadwal_detail.html', context) 

def tambah(request):
    context = {'number':4}
    return render(request, 'home/tambah.html', context)

def tambahSubmisi(request):
    form = JadwalForm()
    if (form.is_valid and request.method == 'POST'):
        form = JadwalForm(request.POST)
        form.save()
        return redirect('/jadwal')

    context = {'form':form}
    return render(request, 'home/jadwal.html', context)

def updateMatkul(request, pk):
    matkul = Jadwal.objects.get(id=pk)
    form = JadwalForm(instance=matkul)

    if (form.is_valid and request.method == 'POST'):
        form = JadwalForm(request.POST, instance=matkul)
        form.save()
        return redirect('/jadwal')

    context = {'form':form,'number':4}
    return render(request, 'home/matkul_update.html', context)

def deleteMatkul(request, pk):
    matkul = Jadwal.objects.get(id=pk)
    
    if request.method == "POST":
        matkul.delete()
        return redirect('/jadwal')


    context = {'matkul':matkul,'number':4}
    return render(request, 'home/matkul_delete.html', context)


