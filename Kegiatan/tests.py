from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from .models import Kegiatan,Peserta
from . import views
from . import models
from .apps import KegiatanConfig
# Create your tests here.

class Tests(TestCase):

    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama_kegiatan="Gas")
        self.peserta = Peserta.objects.create(nama_peserta="kimus",kegiatan=Kegiatan.objects.get(nama_kegiatan="Gas"))
    def test_str(self):
        self.assertEqual(str(self.kegiatan), "Gas")
        self.assertEqual(str(self.peserta), "kimus")
    def test_list_url_is_resolved(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved2(self):
        response = Client().get('/kegiatan/tambahpeserta')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved3(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        self.assertEquals(response.status_code, 200)



    def test_input_tombol_add2(self):
        response = Client().get('/kegiatan/tambahpeserta')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah', html)

    def test_input_tombol_add3(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah', html)

    def test_story6_save_a_POST_request(self):
        response = Client().post('/kegiatan/tambahkegiatan', data={'nama_kegiatan':'Demo Story 6'})
        jumlah = models.Kegiatan.objects.filter(nama_kegiatan='Demo Story 6').count()
        self.assertEqual(jumlah,1)

    def testConfig(self):
        self.assertEqual(KegiatanConfig.name, 'Kegiatan')

    