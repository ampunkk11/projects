from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json
# Create your views here.
def fungsi_url(request):
    response= {"number":7}
    return render(request,'search.html',response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)