from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from .apps import SearchConfig
# Create your tests here.
class Tests(TestCase):
    def test_url_resolved(self):
        response = Client().get('/search/')
        self.assertEquals(response.status_code, 200)
    def test_url_resolved2(self):
        response = Client().get('/search/data/?q=')
        self.assertEquals(response.status_code, 200)
    
class TestApp(TestCase):
    def testConfig(self):
        self.assertEqual(SearchConfig.name, 'search')