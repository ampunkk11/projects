from django.urls import path

from .views import fungsi_url,fungsi_data

app_name = 'search'


urlpatterns = [

    path('', fungsi_url , name = 'search'),
    path('data/', fungsi_data),
]