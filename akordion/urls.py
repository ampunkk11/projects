from django.urls import path
from . import views

app_name = 'akordion'

urlpatterns = [

    path('', views.akordion , name = 'akordion'),

]