from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from .apps import AkordionConfig
# Create your tests here.
class Tests(TestCase):
    def test_url_resolved(self):
        response = Client().get('/akordion/')
        self.assertEquals(response.status_code, 200)

class TestApp(TestCase):
    def testConfig(self):
        self.assertEqual(AkordionConfig.name, 'akordion')

class TestView(TestCase):
    def test_views_akordion(self):
        found = resolve('/akordion/')
        self.assertEqual(found.func, views.akordion)

class TestHTML(TestCase):
    def test_template_used(self):
        response = Client().get('/akordion/')
        self.assertTemplateUsed(response,'akordion/akordion.html')


        