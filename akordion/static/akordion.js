$(document).ready(function() {
    $(".item-header").click(function(){
        if($(this).parent().hasClass('active')){
          $(this).parent().removeClass("active");
        }
        else{
          // $(".accordion-item").removeClass("active");
          $(this).parent().addClass("active");
        ;
      }
      });

      $('.move').children().click(function (e) {
        event.stopPropagation();
        var $div = $(this).closest('.accordion-item');
        if (jQuery(e.target).is('.move-down')) {
            $div.next('.accordion-item').after($div);
        } else {
            $div.prev('.accordion-item').before($div);
        }
    });

});