from django.apps import AppConfig


class AkordionConfig(AppConfig):
    name = 'akordion'
